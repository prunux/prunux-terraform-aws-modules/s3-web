# S3 Buckets for Static Websites and a Bucket for Website Logs
This module creates static S3 Buckets with additional Bucket for Website Logs

## Minimal Example 
Create a S3 Bucket in singapore and create a backup replication S3 Bucket in Frankfurt:

```terraform
module "www-example-com" {
  source                   = "git::ssh://git@gitlab.prunux.ch/infra/terraform-aws-modules/s3-web.git?ref=v1.0.0"
  bucket_name              = "www-example-com
  bucket_access_user_names = ["bucket-prod-user"]
  access_key               = "${module.common.access_keys[var.customer_account]}"
  secret_key               = "${module.common.secret_keys[var.customer_account]}"

  providers = {
    aws.src  = "aws.frankfurt"
  }
}
```



## More Enhanced Example with all arguments

```terraform
module "www-example-com" {
  source                    = "git::ssh://git@gitlab.prunux.ch/infra/terraform-aws-modules/s3-web.git?ref=v1.0.0"
  bucket_name               = "www-example-com
  bucket_region             = "eu-central-1"
  bucket_storage_class      = "STANDARD"
  bucket_access_user_names  = ["bucket-prod-user"]
  bucket_access_roles_names = ["bucket-prod-role"]
  bucket_force_destroy      = false
  access_key                = "${module.common.access_keys[var.customer_account]}"
  secret_key                = "${module.common.secret_keys[var.customer_account]}"

  providers = {
    aws.src  = "aws.frankfurt"
  }

  extra_tags = {
    "Environment" = "Dev",
    "Squad"       = "Ops"  
  }

}
```

All Parmeters:

* `bucket_name`: S3 bucket name
* `bucket_region`: AWS region of S3 bucket
* `bucket_storage_class`: storage class of S3 bucket
* `bucket_access_user_names`: a list of user names that need access to the buckets
* `bucket_access_role_names`: a list of role names that need access to the buckets
* `bucket_force_destroy`: S3 bucket force destroy on destruction
* `access_key`: Programatic Access to AWS account
* `secret_key`: Programatic Access to AWS account

* `providers`: used to pass different AWS regions


## Add IAM user for access
```terraform
resource "aws_iam_user" "bucket-prod" {
  name = "bucket-prod"
}

resource "aws_iam_access_key" "bucket-prod" {
  user = "${aws_iam_user.bucket-prod.name}"
}

output "s3-bucket-prod_key_id" {
  value = "${aws_iam_access_key.bucket-prod.id}"
}

output "s3-bucket-prod_secret_key" {
  value = "${aws_iam_access_key.bucket-prod.secret}"
}
```

Get the credentials and send them to the customer.

```
$ terraform output -module=s3-web
[...]
s3-bucket-prod_key_id = AKIAJUO6HXXXXXXX
s3-bucket-prod_secret_key = 9kOFcvujIlWcXXXXXXXgVDOxV8gPhiO31Hb8
```

## Encryption

All created S3 *Log* Buckets are using the AWS-KMS encryption

```terraform
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "aws:kms"
      }
    }
  }
```
